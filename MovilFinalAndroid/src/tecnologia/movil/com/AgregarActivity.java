package tecnologia.movil.com;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AgregarActivity extends Activity {
 
    private ProgressDialog pDialog;
 
    static String URL ="http://10.0.2.2:8008/Data_android_Final";
    
    JSONParser jsonParser = new JSONParser();
    EditText inputName;
    EditText inputEmail;
    EditText inputDesc;
 
	private static String url_agregar_elemento = URL+"/create.php";
 
    private static final String TAG_SUCCESS = "success";
 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar);

        inputName = (EditText) findViewById(R.id.inputName);
        inputEmail = (EditText) findViewById(R.id.inputEmail);
        inputDesc = (EditText) findViewById(R.id.inputDesc);
 
        Button btnCreate = (Button) findViewById(R.id.btnCreatePendaftaran);
 
        btnCreate.setOnClickListener(new View.OnClickListener() {
 
            @Override
            public void onClick(View view) {
                new CreateElement().execute();
            }
        });
    }

    class CreateElement extends AsyncTask<String, String, String> {
 
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(AgregarActivity.this);
            pDialog.setMessage("Agregando...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }
 
        protected String doInBackground(String... args) {
            String name = inputName.getText().toString();
            String email = inputEmail.getText().toString();
            String description = inputDesc.getText().toString();
 
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("name", name));
            params.add(new BasicNameValuePair("email", email));
            params.add(new BasicNameValuePair("description", description));
 
            JSONObject json = jsonParser.makeHttpRequest(url_agregar_elemento,"POST", params);
 
            try {
                int success = json.getInt(TAG_SUCCESS);
 
                if (success == 1) {
                    Intent i = new Intent(getApplicationContext(), ListarActivity.class);
                    startActivity(i);
 
                    finish();
                } else {
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
 
            return null;
        }
 
        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }
 
    }
}