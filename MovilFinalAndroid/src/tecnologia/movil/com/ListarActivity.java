package tecnologia.movil.com;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
 
public class ListarActivity extends ListActivity {
 static String URL = "http://10.0.2.2:8008/Data_android_Final";
    private ProgressDialog pDialog;
 
    JSONParser jParser = new JSONParser();
 
    ArrayList<HashMap<String, String>> listaElementos;
 
    private static String url_getLista = URL+"/get_all.php";
 
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_PENDAFTARAN = "pendaftaran";
    private static final String TAG_PID = "pid";
    private static final String TAG_NAME = "name";
 
    JSONArray json_array = null;
 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar);
 
        listaElementos = new ArrayList<HashMap<String, String>>();
 
        new CargarDatos().execute();
 
    }
 
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 100) {
            Intent intent = getIntent();
            finish();
            startActivity(intent);
        }
 
    }
 
    class CargarDatos extends AsyncTask<String, String, String> {
 
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ListarActivity.this);
            pDialog.setMessage("Cargando Datos...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }
 
        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            JSONObject json = jParser.makeHttpRequest(url_getLista, "GET", params);
 
            try {
                int success = json.getInt(TAG_SUCCESS);
 
                if (success == 1) {
                    json_array = json.getJSONArray(TAG_PENDAFTARAN);
 
                    for (int i = 0; i < json_array.length(); i++) {
                        JSONObject c = json_array.getJSONObject(i);
 
                        String id = c.getString(TAG_PID);
                        String name = c.getString(TAG_NAME);
 
                        HashMap<String, String> map = new HashMap<String, String>();
 
                        map.put(TAG_PID, id);
                        map.put(TAG_NAME, name);
 
                        listaElementos.add(map);
                    }
                } else {
                    Intent i = new Intent(getApplicationContext(), AgregarActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
 
            return null;
        }
 
        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            runOnUiThread(new Runnable() {
                public void run() {
                    ListAdapter adapter = new SimpleAdapter(
                            ListarActivity.this, listaElementos,
                            R.layout.list_layout, new String[] { TAG_PID,
                                    TAG_NAME},
                            new int[] { R.id.pid, R.id.name });
                    setListAdapter(adapter);
                }
            });
 
        }
 
    }
}